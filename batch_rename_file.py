import os
import shutil
import argparse
import sys
parser = argparse.ArgumentParser()
parser.add_argument("-sn","--subject_name",dest="subjectName",default="", help="put in the subject name for replacing",required=True)
parser.add_argument("-rl","--replace_len",dest="replaceLen",default=-1, help="character len to replace")
parser.add_argument("-td","--target_directory",dest="targetDir",default="", help="target directory to change")
parser.add_argument("-nd","--new_dir_name",dest="newDirName", default="",help="new directory name")
parser.add_argument("-e","--extension",dest="extension", help="specify file type to replace", required=True, nargs='+')
args = parser.parse_args()
cur_dir = os.getcwd()
target_dir = cur_dir # target directory to change
replace_len = len(args.subjectName)
extension_bank_set = set(args.extension)
new_dir_name = "ALL_FILES"
if len(args.targetDir) != 0:
	target_dir = args.targetDir
if args.replaceLen != -1:
	replace_len = args.replaceLen
if len(args.newDirName) != 0:
	new_dir_name = args.newDirName
new_dir = os.path.join(cur_dir,new_dir_name)


# Incase new_dir is created
try:
	os.mkdir(new_dir)
except:
	pass
for subdir, dirs, files in os.walk(target_dir):
	for file in files:
		file_extension = os.path.splitext(file)
		if subdir != new_dir and file_extension[1] in extension_bank_set:
			temp = args.subjectName+file[replace_len:]
			src = os.path.join(subdir, file)
			dest = os.path.join(new_dir,temp)
			shutil.copyfile(src,dest)



		# print(subdir + '/' + file